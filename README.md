## Weather Reporter
This app is created using React and Redux

## Installation

Run the below command in terminal

### `npm install`

This will install all the dependencies<br>

### `npm run build`

## Start

### `npm run start:dev`

Open [http://localhost:8080](http://localhost:8080) to view it in the browser.<br>
Note: The weather data will be auto-updated every 60 seconds.
Data Source: [https://openweathermap.org](https://openweathermap.org)

#### Tested in macOs High Sierra V10.13.5

<img src="https://gitlab.com/rajzone/WeatherReporter/raw/master/LoadingSnapshot.png" />

<img src="https://gitlab.com/rajzone/WeatherReporter/raw/master/WeatherSnapshot.png" />
