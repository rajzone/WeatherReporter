// ActionTypes are defined in constants.

// They are imported in Actions and Reducers.

/* eslint-disable */

export const FETCH_DATA_FULFILLED = "FETCH_DATA_FULFILLED";
export const FETCH_DATA_REJECTED = "FETCH_DATA_REJECTED";

/* eslint-enable */
